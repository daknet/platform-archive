<?php

namespace Daknet\Platform\Tests;

use Daknet\Platform\Contracts\Database\CurrencyRepository;
use Daknet\Platform\Database\Models\AdminUser;
use Daknet\Platform\Database\Models\Currency;
use Daknet\Platform\PlatformServiceProvider;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Notification;
use Orchestra\Testbench\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    /**
     * @var \Daknet\Platform\Database\Models\AdminUser
     */
    protected $user;
    
    /**
     * @var \Faker\Generator
     */
    protected $faker;
    
    /**
     * Setup the test environment
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    
        $this->faker = $this->app->make(Faker::class);
    
        $this->withFactories(__DIR__.'/../database/factories');
    
        $this->setUpDatabase();
        $this->setDefaultCurrency();
        
        Notification::fake();
    }
    
    /**
     * Define the environment setup
     *
     * @param \Illuminate\Foundation\Application $app
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('app.key', 'base64:+t+SbKSCJf/ICk91/holnMeg7QoEaVBdR/xa3vGvCwU=');
    }
    
    /**
     * Get the package providers
     *
     * @param \Illuminate\Foundation\Application $app
     *
     * @return array
     */
    protected function getPackageProviders($app): array
    {
        return [
            PlatformServiceProvider::class
        ];
    }
    
    /**
     * Get package aliases
     *
     * @param \Illuminate\Foundation\Application $app
     *
     * @return array
     */
    protected function getPackageAliases($app)
    {
        return [
            'Breadcrumb' => \Daknet\Platform\Support\Facades\Breadcrumb::class
        ];
    }
    
    protected function setUpDatabase(): void
    {
        $this->loadLaravelMigrations();
        $this->resetDatabase();
    }
    
    protected function resetDatabase()
    {
        $this->artisan('migrate');
    }
    
    protected function setDefaultCurrency()
    {
        factory(Currency::class)->create();
        
        $repository = app(CurrencyRepository::class);
        $currency = $repository->all()->first();
    
        $this->withSession([
            'default_session' => $currency
        ]);
    }
    
    protected function createAdminUser($data = ['is_super_admin' => 1])
    {
        if (null === $this->user) {
            $this->user = factory(AdminUser::class)->create($data);
        }
        
        return $this;
    }
}