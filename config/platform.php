<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | The Components Admin Path
    |--------------------------------------------------------------------------
    |
    | This is the URI path where Components admin will be accessible. This URI
    | will not affect any paths.
    |
    */
    
    'admin_url' => 'admin',
    
    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */
    
    'symlink_storage' => 'storage',
    
    /*
    |--------------------------------------------------------------------------
    | Cart Session
    |--------------------------------------------------------------------------
    |
    | This option is used to set session to a random, character strings.
    | Please do this before deploying an application.
    |
    */
    
    'cart' => [
        'session_key' => 'cart_products',
        'promotion_key' => 'cart_discount'
    ],
    
    /*
    |--------------------------------------------------------------------------
    | User Model
    |--------------------------------------------------------------------------
    |
    | This is the model in your application that will serve as the primary
    | model.
    |
    */
    
    'model' => [
        'user' => App\User::class
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver.
    |
    */
    
    'filesystems' => [
        'disks' => [
            'platform' => [
                'driver' => 'local',
                'root' => storage_path('app/public')
            ]
        ]
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Image
    |--------------------------------------------------------------------------
    |
    | This option controls the image feature of your application. You may
    | change these as required.
    |
    */
    
    'image' => [
        'driver' => 'gd',
        'sizes' => [
            'small' => ['150', '150'],
            'med' => ['350', '350'],
            'large' => ['750', '750']
        ]
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Authentication
    |--------------------------------------------------------------------------
    |
    | This option controls the authentication "guard" and password reset
    | options for your application. You may change these as required.
    |
    */
    
    'auth' => [
        'guards' => [
            'admin' => [
                'driver' => 'session',
                'provider' => 'admin-users'
            ]
        ],
        
        'providers' => [
            'admin-users' => [
                'driver' => 'eloquent',
                'model' => Daknet\Platform\Database\Models\AdminUser::class
            ]
        ],
        
        'passwords' => [
            'adminusers' => [
                'provider' => 'admin-users',
                'table' => 'admin_password_resets',
                'expire' => 60
            ]
        ]
    ]

];
