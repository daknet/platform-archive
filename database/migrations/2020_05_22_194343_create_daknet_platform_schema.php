<?php

use Daknet\Platform\Database\Models\Country;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaknetPlatformSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->default(null);
            $table->string('code')->nullable()->default(null);
            $table->boolean('is_default')->default(false);
            $table->timestamps();
        });
    
        Schema::create('admin_password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });
    
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->timestamps();
        });
    
        Schema::create('admin_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('is_super_admin')->nullable()->default(null);
            $table->bigInteger('role_id')->unsigned()->default(null);
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('language')->nullable()->default('en');
            $table->string('image_path')->nullable()->default(null);
            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
        
        // categories
        
        Schema::create('permissions', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
        
        Schema::create('permission_role', function(Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('permission_id');
            $table->bigInteger('role_id')->unsigned();
            $table->timestamps();
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
        
        $content = __DIR__.'/../../public/countries.json';
        $countries = json_decode(file_get_contents($content), true);
    
        foreach ($countries as $country) {
            Country::create([
                'code' => strtolower(array_get($country, 'alpha2Code')),
                'name' => array_get($country, 'name'),
                'phone_code' => array_get($country, 'callingCodes.0'),
                'currency_code' => array_get($country, 'currencies.0.code'),
                'currency_symbol' => array_get($country, 'currencies.0.symbol'),
                'lang_code' => array_get($country, 'languages.0.name')
            ]);
        }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        
        Schema::dropIfExists('languages');
        Schema::dropIfExists('admin_password_resets');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('admin_users');
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('permission_role');
        
        Schema::enableForeignKeyConstraints();
    }
}
